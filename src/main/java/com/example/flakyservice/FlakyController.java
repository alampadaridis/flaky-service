package com.example.flakyservice;

import com.example.flakyservice.domain.Person;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FlakyController {

  private final Log log = LogFactory.getLog(FlakyController.class);

  @GetMapping("/hello/{name}")
  public ResponseEntity<Person> hello(@PathVariable String name) {
    log.info("controller called");
    if ("alice".equals(name)) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    else if ("bob".equals(name)) {
      return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
    }
    return new ResponseEntity<Person>(new Person(name), HttpStatus.OK);
  }
}
